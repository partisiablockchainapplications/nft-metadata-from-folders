### run
```bash
cargo run -- --help

# to generate img and json files
cargo run -- -i -j

# to override exiting files
cargo run -- -i -j -o
```