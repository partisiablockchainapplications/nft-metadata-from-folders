use anyhow::{bail, ensure, Result};
use chrono::Utc;
use clap::{crate_version, App, Arg};
use futures::future::join_all;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};
use std::ffi::{OsStr, OsString};
use std::fs::{self, DirEntry};
use std::path::{Path, PathBuf};
use tokio::sync::RwLock;

#[derive(Debug, Deserialize)]
struct Config {
    total_generated: u32,
    cid_prefix: String,
    build_properties: BuildProperties,
    in_layers: Vec<ConfigLayers>,
    nft_properties: NftProperties,
}

#[derive(Debug, Deserialize, Clone)]
struct BuildProperties {
    out_json: String,
    out_images: String,
}

#[derive(Debug, Deserialize, Clone)]
struct NftProperties {
    nft_name: String,
    nft_description: String,
    creator: String,
    trait_type: String,
}

#[derive(Debug, Deserialize)]
struct ConfigLayers {
    name: String,
    iterations: u32,
    preview: Option<String>,
    path: String,
    label: String,
}

#[derive(Debug, Deserialize, Serialize)]
struct NftJson<'a> {
    name: &'a str,
    description: Vec<&'a str>,
    image: &'a str,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename(serialize = "extendInfo"))]
    extend_info: Option<ExtentInfo<'a>>,
    edition: u32,
    date: u64,
    creator: &'a str,
    attributes: Vec<NftAttributes<'a>>,
}

#[derive(Debug, Deserialize, Serialize)]
struct ExtentInfo<'a> {
    #[serde(rename(serialize = "videoUrl"))]
    video_url: &'a str,
}
#[derive(Debug, Deserialize, Serialize)]
struct NftAttributes<'a> {
    trait_type: &'a str,
    value: &'a str,
}
#[derive(Debug)]
struct FileSrc {
    name: String,
    file_type: OsString,
    file_path: PathBuf,
}

lazy_static! {
    static ref CID_PREFIX: RwLock<String> = RwLock::new("".to_string());
    static ref BUILD_JSON: RwLock<bool> = RwLock::new(false);
    static ref BUILD_IMG: RwLock<bool> = RwLock::new(false);
}

#[tokio::main]
async fn main() -> Result<()> {
    let version = crate_version!();
    let cli_args = App::new("partia-block-filler")
        .about("Partia NFT RED")
        .version(version)
        .arg(
            Arg::with_name("override build files")
                .short("o")
                .long("override")
                .help("override build files if they exist")
                .takes_value(false),
        )
        .arg(
            Arg::with_name("build img")
                .short("i")
                .long("build-images")
                .help("build image files")
                .takes_value(false),
        )
        .arg(
            Arg::with_name("build json")
                .short("j")
                .long("build-json")
                .help("build json files")
                .takes_value(false),
        )
        .get_matches();

    let override_files = cli_args.is_present("override build files");
    let build_json = cli_args.is_present("build json");
    let build_img = cli_args.is_present("build img");

    let env_path: PathBuf = [".", ".env.toml"].iter().collect();
    let env_raw: String = fs::read_to_string(env_path).expect("Unable to read Config");
    let config: Config =
        toml::from_str(&env_raw.clone()).expect(".env.toml file is formatted badly");

    *CID_PREFIX.write().await = config.cid_prefix.to_string();
    *BUILD_JSON.write().await = build_json;
    *BUILD_IMG.write().await = build_img;

    // confirm the totals
    ensure!(
        config.in_layers.iter().map(|l| l.iterations).sum::<u32>() == config.total_generated,
        "mismatch total_generated number"
    );

    // delete and create the folder
    if override_files && build_json {
        fs::remove_dir_all(&config.build_properties.out_json).unwrap_or(());
    }
    if build_json {
        fs::create_dir_all(&config.build_properties.out_json)?;
    }
    if override_files && build_img {
        fs::remove_dir_all(&config.build_properties.out_images).unwrap_or(());
    }
    if build_img {
        fs::create_dir_all(&config.build_properties.out_images)?;
    }

    // get a list of files in path
    let mut vec_path_images = Vec::with_capacity(config.in_layers.len());
    for layer in &config.in_layers {
        let dir = fs::read_dir(&layer.path)?;
        if let Some(file) = dir.into_iter().next() {
            let file: DirEntry = file?;
            vec_path_images.push(FileSrc {
                file_type: Path::new(&file.path())
                    .extension()
                    .unwrap_or_default()
                    .to_os_string(),
                file_path: file.path(),
                name: layer.name.clone(),
            })
        } else {
            bail!("no file found in {}", layer.path);
        }
    }

    // generate the files
    let mut vec_threads: Vec<_> = Vec::with_capacity(config.total_generated.try_into()?);
    let mut edition: u32 = 0;
    for (idx, layer) in config.in_layers.iter().enumerate() {
        for _ in 0..layer.iterations {
            let file_ext = vec_path_images[idx].file_type.clone();
            let file_path = vec_path_images[idx].file_path.clone();
            let name = vec_path_images[idx].name.clone();
            edition += 1;

            let layer_move = layer.label.clone();
            let preview = layer.preview.clone();
            let nft_properties = config.nft_properties.clone();
            let build_properties = config.build_properties.clone();
            let task = tokio::spawn(async move {
                generate_nft(
                    edition,
                    &name,
                    file_ext.as_os_str(),
                    file_path.as_os_str(),
                    preview,
                    layer_move,
                    &nft_properties,
                    &build_properties,
                )
                .await
            });
            vec_threads.push(task);
        }
    }
    let res = join_all(vec_threads).await;

    let (success, errors): (Vec<_>, Vec<_>) = res.into_iter().partition(Result::is_ok);
    errors
        .into_iter()
        .for_each(|e| println!("join error: {:?}", e));

    let success: Vec<_> = success.into_iter().map(Result::unwrap).collect();
    let (success, errors): (Vec<_>, Vec<_>) = success.into_iter().partition(Result::is_ok);

    let success: Vec<_> = success.into_iter().map(Result::unwrap).collect();
    let errors: Vec<_> = errors.into_iter().map(Result::unwrap_err).collect();

    println!("success created files: {}", success.len());
    errors.into_iter().for_each(|e| println!("{}", e));
    Ok(())
}

async fn generate_nft(
    edition: u32,
    name: &str,
    file_ext: &OsStr,
    file_path: &OsStr,
    preview: Option<String>,
    label: String,
    nft_properties: &NftProperties,
    build_properties: &BuildProperties,
) -> Result<()> {
    let file_ext = file_ext.to_str().unwrap_or_default();
    let path_img: PathBuf = [
        &build_properties.out_images,
        &[name, ".", file_ext].concat(),
    ]
    .iter()
    .collect();

    let path_json: PathBuf = [
        &build_properties.out_json,
        &[&edition.to_string(), ".json"].concat(),
    ]
    .iter()
    .collect();

    let edition_string = edition.to_string();
    let dt = Utc::now();
    let timestamp: i64 = dt.timestamp();
    let cid_prefix = &*CID_PREFIX.read().await;

    let file_path_nft = ["ipfs://", &cid_prefix, "/", name, ".", file_ext].concat();
    let (extend_info, image) = if file_ext == "mp4" {
        ensure!(preview.is_some());
        (
            Some(ExtentInfo {
                video_url: &file_path_nft,
            }),
            preview.unwrap(),
        )
    } else {
        (
            None,
            file_path_nft,
        )
    };

    let json = NftJson {
        name: &[nft_properties.nft_name.as_str(), &edition_string].concat(),
        description: vec![&nft_properties.nft_description],
        image: &image,
        edition,
        date: u64::try_from(timestamp)? * 1000,
        creator: &nft_properties.creator,
        attributes: vec![NftAttributes {
            trait_type: &nft_properties.trait_type,
            value: &label,
        }],
        extend_info,
    };

    // do not override
    if !Path::new(path_json.as_os_str()).exists() && *BUILD_JSON.read().await {
        fs::write(path_json.as_os_str(), serde_json::to_string_pretty(&json)?)?;
    }
    if !Path::new(path_img.as_os_str()).exists() && *BUILD_IMG.read().await {
        // fs::write(path_img.as_os_str(), Vec::new())?;
        fs::copy(file_path, path_img.as_os_str())?;
    }
    println!("edition complete {}", edition);
    Ok(())
}
